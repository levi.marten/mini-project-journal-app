const ENTRIES_URL = 'http://localhost:3000/v1/api/entries';

let elSubmitBtn = document.getElementById('submit-btn');
elSubmitBtn.addEventListener('click', () => {
    postEntry();
    alert("Entry added! Great success! 😎")
});

let postEntry = () => {

    let elTitle = document.getElementById('title').value;
    let elContent = document.getElementById('content').value;
    let elAuthorId = document.getElementById('select-author').value.split('')[0];
    let elJournalId = document.getElementById('select-journal').value.split('')[0];

    let data = {
        "title": `"${elTitle}"`,
        "content": `"${elContent}"`,
        "author_id": `${elAuthorId}`,
        "journal_id": `${elJournalId}`
    };
    fetch(ENTRIES_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.error(error);
        });

};
