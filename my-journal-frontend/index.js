const ENTRIES_URL = 'http://localhost:3000/v1/api/entries';
let elEntriesMain = document.getElementById("entries");

fetch(ENTRIES_URL)
    .then(resp => resp.json())
    .then(data => {
        console.log(data);
        data.forEach(response => {
            let post = document.createElement("div");
            post.className = "post";
            post.innerHTML = `<h2>${response.title}</h2> <p>${response.content}</p>`; 
            elEntriesMain.appendChild(post);
        });
    })
    .catch(err => {
        console.error(err);
    });


