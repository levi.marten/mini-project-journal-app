const JOURNALS_URL = 'http://localhost:3000/v1/api/journals';
let elJournalsMain = document.getElementById("journals");

fetch(JOURNALS_URL)
    .then(resp => resp.json())
    .then(data => {
        console.log(data);
        data.forEach(response => {
            let journal = document.createElement("div");
            journal.className = "journal";
            journal.innerHTML = `<h2>${response.name}</h2>`; 
            elJournalsMain.appendChild(journal);
        });
    })
    .catch(err => {
        console.error(err);
    });
