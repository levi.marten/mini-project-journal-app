const AUTHORS_URL = 'http://localhost:3000/v1/api/authors';
const JOURNALS_URL = 'http://localhost:3000/v1/api/journals';

let elSelectAuthor = document.getElementById('select-author')
let elSelectJournal = document.getElementById('select-journal')


fetch(AUTHORS_URL)
    .then(resp => resp.json())
    .then(data => {
        console.log(data);
        data.forEach(response => {

            let author = document.createElement('option');
            author.className = "option";
            author.innerHTML = `${response.author_id} ${response.first_name} ${response.last_name}`;
            elSelectAuthor.appendChild(author);

        });
    })
    .catch(err => {
        console.error(err);
    });

fetch(JOURNALS_URL)
    .then(resp => resp.json())
    .then(data => {
        console.log(data);
        data.forEach(response => {

            let journal = document.createElement('option');
            journal.className = "option";
            journal.innerHTML = `${response.journal_id} ${response.name}`;
            elSelectJournal.appendChild(journal);
        });
    })
    .catch(err => {
        console.error(err);
    });


   // let noDups = Array.from(new Set(response))