## Welcome to the awesome journal app that I designed as a n educational project.

This journal app helps me practice complete integration of backend to frontend, with databases. Fullstack baby.


## Database
Using MySQL examples provided by @SumoDevelopment (teacher) and served with Sequelize, dotenv and mysql2 on Node.js. 

## Server
Using node.js with Express to serve an API. Using Sequelize to access the database.

## Frontend
See frontend documentation. 