CREATE DATABASE myjournal;
USE myjournal;

-- journal 
CREATE TABLE journal (
	journal_id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(80) NOT NULL UNIQUE,
    created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);
SELECT * FROM journal;

-- author
CREATE TABLE author (
	author_id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(80) NOT NULL,
    last_name VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL UNIQUE,
    created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);
SELECT * FROM author;

-- tag 
CREATE TABLE tag (
	tag_id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(40) NOT NULL,
	created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);
SELECT * FROM tag;

-- journal_entry
CREATE TABLE journal_entry (
	journal_entry_id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(128) NOT NULL,
    content TEXT, 
    created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW(),
    journal_id INTEGER NOT NULL,
    FOREIGN KEY (journal_id) REFERENCES journal(journal_id),
    author_id INTEGER NOT NULL,
    FOREIGN KEY (author_id) REFERENCES author(author_id)
);
SELECT * FROM journal_entry;

-- Linking Table:
CREATE TABLE journal_entry_tag (
	journal_entry_tag_id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    journal_entry_id INTEGER NOT NULL,
    tag_id INTEGER NOT NULL,
    FOREIGN KEY (journal_entry_id) REFERENCES journal_entry(journal_entry_id),
    FOREIGN KEY (tag_id) REFERENCES tag(tag_id),
    created_at DATETIME DEFAULT NOW() NOT NULL,
    updated_at DATETIME DEFAULT NULL ON UPDATE NOW()
);
SELECT * FROM journal_entry_tag;




