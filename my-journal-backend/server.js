// Plugins used and initialized -------------------------------------------------------------------
const { Sequelize, QueryTypes } = require('sequelize');
const express = require('express');
const app = express();
const { PORT = 3000 } = process.env;
const path = require('path');
const cors = require('cors');
require('dotenv').config();
const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }))

// Server logic -----------------------------------------------------------------------------------

// Journals
// Get all journals
app.get('/v1/api/journals', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        let data = await sequelize.query('SELECT * FROM journal', {
            type: QueryTypes.SELECT
        });

        return res.status(200).json(data);
    }
    catch (e) {
        console.error(e);
    };
});

// Authors
app.get('/v1/api/authors', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        let data = await sequelize.query('SELECT * FROM author', {
            type: QueryTypes.SELECT
        });

        return res.status(200).json(data);
    }
    catch (e) {
        console.error(e);
    };
});

// Tags 
app.get('/v1/api/tags', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        let data = await sequelize.query('SELECT * FROM tag', {
            type: QueryTypes.SELECT
        });

        return res.status(200).json(data);
    }
    catch (e) {
        console.error(e);
    };
});

// Entries
// Get all entries
app.get('/v1/api/entries', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        let data = await sequelize.query(`SELECT * FROM journal_entry`, {
            type: QueryTypes.SELECT
        });

        return res.status(200).json(data);
    }
    catch (e) {
        console.error(e);
    };
});

// Post a new entry
app.post('/v1/api/entries', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        await sequelize.query(`INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES (:title, :content, :author_id, :journal_id)`, {
            replacements: { title: req.body.title, content: req.body.content, author_id: req.body.author_id, journal_id: req.body.journal_id },
            type: QueryTypes.INSERT
        });

        const name = await sequelize.query('SELECT name FROM journal WHERE journal_id = :journal_id', {
            replacements: { journal_id: req.body.journal_id },
            type: QueryTypes.SELECT
        });

        return res.status(201).send(`Entry added to journal ${Object.values(name[0])[0]}.`)
    }
    catch (e) {
        console.error(e);
    };
});

// Linking table for tags and entries
app.get('/v1/api/entry-tags', async (req, res) => {
    try {
        await sequelize.authenticate();
        console.log('Connection established...');

        let data = await sequelize.query(`SELECT * FROM journal_entry_tag`, {
            type: QueryTypes.SELECT
        });

        return res.status(200).json(data);
    }
    catch (e) {
        console.error(e);
    };
});

app.listen(PORT, () => console.log(`Server started on port ${PORT}...`));